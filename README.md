# Stateful Machines

Resources for my Swift×NW talk.

## Abstract

As app developers we have to deal with the challenges of stateful code every day. 
State machines are a powerful technique for managing state — but they’re underused 
in Mac and iOS development, in part because they can be awkward to implement in 
Objective-C. Swift changes that. Swift’s smart enums make implementing state machines 
quick, easy, and fun. In this talk we’ll look at how you can make your apps easier to 
write, debug, and evolve using state machines in Swift. Along the way, we’ll explore 
the power of enums, including associated values, methods, and properties.
