//
//  TabBarViewController.swift
//  StatefulMachines
//
//  Created by Curt Clifton on 9/9/17.
//  Copyright © 2017 Curt Clifton. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {

    
    override var selectedViewController: UIViewController? {
        didSet {
            guard let destination = selectedViewController else { return }
            switch destination {
            case let imageCollection as ImageCollectionViewController:
                imageCollection.imageSet = nil
                imageVendor.fetchImageSet({ (imageSet) in
                    imageCollection.imageSet = imageSet
                })
            case is AudioPlaybackViewController:
                break
            case is CheckMarkViewController:
                break
            default:
                assertionFailure("unexpected destination")
            }
        }
    }
    
    // MARK: Private API
    
    private let imageVendor = ImagesVendor()
}
