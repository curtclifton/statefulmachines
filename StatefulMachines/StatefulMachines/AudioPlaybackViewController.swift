//
//  AudioPlaybackViewController.swift
//  StatefulMachines
//
//  Created by Curt Clifton on 9/9/17.
//  Copyright © 2017 Curt Clifton. All rights reserved.
//

import UIKit
import AVKit

class AudioPlaybackViewController: UIViewController {
    @IBOutlet weak var playbackPosition: UISlider!
    @IBOutlet weak var playPauseButton: UIButton!
    @IBOutlet weak var stopButton: UIButton!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var imageContainer: UIView!
    @IBOutlet weak var idleImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // please excuse these !s for demo's sake
        let narwhals = Bundle.main.url(forResource: "narwhalsClip", withExtension: "m4a")!
        player = try! AVAudioPlayer(contentsOf: narwhals)
        player.delegate = self
        player.prepareToPlay()
        
        timeLabel.makeLabelMonospaced()
        stateLabel.makeLabelMonospaced()
        
        imageContainer.layer.cornerRadius = 10
        
        stateLabel.isHidden = !Parameters.shouldShowStateLabel
        
        updateControls()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        state = .reset
    }
    
    // MARK: Actions
    
    @IBAction func playPauseTapped(_ sender: UIButton) {
        if sender.currentTitle == "Play" {
            play()
        } else {
            pause()
        }
    }
    
    @IBAction func pleaseStopTapped(_ sender: UIButton) {
        stop()
    }
    
    @IBAction func scrubberTouchDown(_ sender: UISlider) {
        sliderTouchDown()
    }
    
    @IBAction func scrubberTouchUp(_ sender: UISlider) {
        sliderTouchUp()
    }
    
    @IBAction func scrubbed(_ sender: UISlider) {
        let position = PlaybackPosition(slider: playbackPosition)
        sliderValueChanged(to: position)
    }
    
    // MARK: Private API
    private var player: AVAudioPlayer! {
        didSet {
            let thumbDiameter: CGFloat = 28 // empirical value for iOS 11
            let points = playbackPosition.bounds.width - thumbDiameter
            let duration = player.duration
            let pointsPerSecond = Double(points) / duration
            timerFrequency = 1 / pointsPerSecond
        }
    }
    
    private var elapsedPlayback: PlaybackPosition {
        get {
            return PlaybackPosition(player: player)
        }
        set {
            let newTime = player.duration * Double(newValue.percent)
            player.currentTime = newTime
        }
    }
    
    private var state: PlaybackState = .idle {
        didSet {
            guard (state != oldValue) else { return }
            
            // On Exit actions
            switch oldValue {
            case .reset:
                break
            case .idle:
                break
            case .playing:
                stopTimeUpdateTimer()
            case .paused:
                break
            case let .scrubbing(motion: motion, history: _, position: _):
                elapsedPlayback = PlaybackPosition(slider: playbackPosition)
                switch motion {
                case .held(.playing):
                    stopScrubbingUpdateTimer()
                case .held(.paused):
                    break
                case .dragging:
                    stopScrubbingDragSettlingTimer()
                }
            }
            
            // On Enter actions
            switch state {
            case .reset:
                player.stop()
                player.currentTime = 0
                updateTimes(animated: false)
                thenSetState(to: .idle)
            case .idle:
                player.prepareToPlay()
            case .playing:
                startTimeUpdateTimer()
                player.play()
            case .paused:
                // no on-enter actions
                break
            case .scrubbing(motion: .held(.playing), history: _, position: _):
                startScrubbingUpdateTimer()
            case .scrubbing(motion: .held(.paused), history: _, position: _):
                // no on-enter actions
                break
            case .scrubbing(motion: .dragging, history: _, position: _):
                startScrubbingDragSettlingTimer()
                player.pause()
            }
            
            updateControls()
        }
    }
    private var isNearEnd: Bool {
        return playbackPosition.value > 0.99
    }

    private func thenSetState(to state: PlaybackState) {
        DispatchQueue.main.async {
            self.state = state
        }
    }
    
    // MARK: Events
    
    private func play() {
        if isNearEnd {
            player.currentTime = 0
            playbackPosition.value = 0
        }
        state = .playing
    }
    
    private func pause() {
        player.pause()
        state = .paused
    }
    
    private func stop() {
        state = .reset
    }
    
    private func didFinish() {
        switch state {
        case .playing:
            updateTimes(animated: true)
            state = .paused
        case let .scrubbing(motion: .held(.playing), history: history, position: position):
            updateTimeLabel(animated: true)
            state = .scrubbing(motion: .held(.paused), history: history, position: position)
        default:
            break
        }
    }
    
    private func sliderTouchDown() {
        switch state {
        case .playing:
            state = .scrubbing(motion: .held(.playing), history: .playing, position: PlaybackPosition(slider: playbackPosition))
        case .paused:
            state = .scrubbing(motion: .held(.paused), history: .paused, position: PlaybackPosition(slider: playbackPosition))
        default:
            break
        }
    }
    
    private func sliderTouchUp() {
        switch state {
        case .scrubbing(motion: _, history: .playing, position: _):
            if isNearEnd {
                state = .paused
            } else {
                state = .playing
            }
            
        case .scrubbing(motion: _, history: .paused, position: _):
            state = .paused
            
        default:
            break
        }
    }
    
    private func sliderValueChanged(to newPosition: PlaybackPosition) {
        switch state {
        case .scrubbing(motion: .held(_), history: let history, position: let previousPosition)
            where !newPosition.approximatelyEqual(previousPosition):
            updateTimeLabel(animated: false, to: newPosition)
            state = .scrubbing(motion: .dragging, history: history, position: newPosition)
            
        case .scrubbing(motion: .dragging, history: let history, position: let previousPosition)
            where !newPosition.approximatelyEqual(previousPosition):
            updateTimeLabel(animated: false, to: newPosition)
            state = .scrubbing(motion: .dragging, history: history, position: newPosition)
            
        case .scrubbing(motion: .dragging, history: let history, position: _): // approximately equal
            if history == .paused || isNearEnd {
                updateTimeLabel(animated: false, to: newPosition)
                state = .scrubbing(motion: .held(.paused), history: history, position: newPosition)
            } else {
                elapsedPlayback = newPosition
                player.play()
                state = .scrubbing(motion: .held(.playing), history: history, position: newPosition)
            }
        default:
            break
        }
    }
    
    // MARK: Timer Management
    
    private var timeUpdateTimer: Timer?
    private var timerFrequency: TimeInterval = 0.5
    private var scrubbingUpdateTimer: Timer?
    private var scrubbingDragSettlingTimer: Timer?
    
    private func startTimeUpdateTimer() { // Timer 1
        assert(timeUpdateTimer == nil)
        
        let newTimer = Timer.scheduledTimer(withTimeInterval: timerFrequency, repeats: true, block: { _ in
            assert(self.state == .playing)
            self.updateTimes(animated: true)
        })
        timeUpdateTimer = newTimer
    }
    
    private func stopTimeUpdateTimer() { // Timer 1
        timeUpdateTimer?.invalidate()
        timeUpdateTimer = nil
    }
    
    private func startScrubbingUpdateTimer() { // Timer 2
        assert(scrubbingUpdateTimer == nil)
        
        let newTimer = Timer.scheduledTimer(withTimeInterval: timerFrequency, repeats: true, block: { _ in
            switch self.state {
            case .scrubbing(motion: .held(.playing), history: _, position: _):
                self.updateTimeLabel(animated: false)
            default:
                assertionFailure()
            }
        })
        scrubbingUpdateTimer = newTimer
    }
    
    private func stopScrubbingUpdateTimer() { // Timer 2
        scrubbingUpdateTimer?.invalidate()
        scrubbingUpdateTimer = nil
    }
    
    private func startScrubbingDragSettlingTimer() { // Timer 3
        assert(scrubbingDragSettlingTimer == nil)
        
        let newTimer = Timer.scheduledTimer(withTimeInterval: Parameters.scrubbingSettlingTime, repeats: true, block: { _ in
            switch self.state {
            case .scrubbing(motion: .dragging, history: _, position: _):
                self.sliderValueChanged(to: PlaybackPosition(slider: self.playbackPosition))
            default:
                assertionFailure()
            }
        })
        scrubbingDragSettlingTimer = newTimer
    }
    
    private func stopScrubbingDragSettlingTimer() { // Timer 3
        scrubbingDragSettlingTimer?.invalidate()
        scrubbingDragSettlingTimer = nil
    }
    
    // MARK: Display Management
    
    private func updateControls() {
        updateButtons()
        updateStateLabel()
        updateIdleImage()
    }
    
    private func updateButtons() {
        let title: String
        let enable: Bool
        switch state {
        case .idle, .paused:
            title = "Play"
            enable = true
        case .playing:
            title = "Pause"
            enable = true
        case .scrubbing(motion: _, history: .playing, position: _):
            title = "Pause"
            enable = false
        default:
            title = "Play"
            enable = false
        }
        playPauseButton.setTitle(title, for: .normal)
        playPauseButton.isEnabled = enable
        
        stopButton.isHidden = state.isIdle
    }
    
    private func updateStateLabel() {
        stateLabel.text = String(describing: state)
    }
    
    private func updateTimes(animated: Bool) {
        updateTimeLabel(animated: animated)
        updateScrubberTime()
    }
    
    private func updateTimeLabel(animated: Bool, to position: PlaybackPosition? = nil) {
        let timeToShow: TimeInterval
        if let position = position {
            timeToShow = Double(position.percent) * player.duration
        } else {
            timeToShow = player.currentTime
        }
        
        let currentTimeToNearestSecond = Int(round(timeToShow))
        
        // totally cheating because I know the audio clip is < 1 minute
        let timeText: String
        if currentTimeToNearestSecond < 10 {
            timeText = "0:0\(currentTimeToNearestSecond)"
        } else {
            timeText = "0:\(currentTimeToNearestSecond)"
        }
        if animated {
            UIView.transition(with: timeLabel, duration: timerFrequency, options: .transitionCrossDissolve, animations: {
                self.timeLabel.text = timeText
            }, completion: nil)
        } else {
            timeLabel.text = timeText
        }
    }
    
    private func updateScrubberTime() {
        playbackPosition.value = elapsedPlayback.percent
    }
    
    private func updateIdleImage() {
        UIView.animate(withDuration: 0.1) {
            self.idleImageView.alpha = (self.state.isIdle ? 1 : 0)
        }
    }
}

extension AudioPlaybackViewController: AVAudioPlayerDelegate {
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        didFinish()
    }
}

private enum PlaybackState {
    case reset
    case idle
    case playing
    case paused
    case scrubbing(motion: ScrubbingMotion, history: PlayingSubstate, position: PlaybackPosition)
    
    var isIdle: Bool {
        switch self {
        case .idle, .reset: return true
        default: return false
        }
    }
}

private enum ScrubbingMotion {
    case held(PlayingSubstate)
    case dragging
}

private enum PlayingSubstate {
    case playing
    case paused
}

extension PlaybackState: Equatable {
    static func ==(lhs: PlaybackState, rhs: PlaybackState) -> Bool {
        switch (lhs, rhs) {
        case (.reset, .reset): return true
        case (.idle, .idle): return true
        case (.playing, .playing): return true
        case (.paused, .paused): return true
        case let (.scrubbing(lhsMotion, lhsHistory, lhsPosition), .scrubbing(rhsMotion, rhsHistory, rhsPosition))
            where lhsMotion == rhsMotion && lhsHistory == rhsHistory && lhsPosition.approximatelyEqual(rhsPosition):
            return true
        default: return false
        }
    }
}

extension ScrubbingMotion: Equatable {
    static func ==(lhs: ScrubbingMotion, rhs: ScrubbingMotion) -> Bool {
        switch (lhs, rhs) {
        case let (.held(lhsSubstate), .held(rhsSubstate)) where lhsSubstate == rhsSubstate:
            return true
        case (.dragging, .dragging): return true
        default: return false
        }
    }
}

extension PlaybackState: CustomStringConvertible {
    var description: String {
        switch self {
        case .reset: return "reset"
        case .idle: return "idle"
        case .playing: return "playing"
        case .paused: return "paused"
        case .scrubbing(let motion, let history, let position):
            switch (motion, history) {
            case (.dragging, .playing):
                return "at \(position)—was playing"
            case (.held(.playing), .playing):
                return "held ▶️ \(position)—was playing"
            case (.held(.paused), .playing):
                return "held ⏸ \(position)—was playing"
            case (.dragging, .paused):
                return "at \(position)—was paused"
            case (.held(.playing), .paused):
                return "held ▶️ \(position)—was paused"
            case (.held(.paused), .paused):
                return "held ⏸ \(position)—was paused"
            }
        }
    }
}

private struct PlaybackPosition {
    let percent: Float
    
    init(slider: UISlider) {
        percent = slider.value
        assert(0 <= percent && percent <= 1.0)
    }
    
    init(player: AVAudioPlayer) {
        percent = Float(player.currentTime / player.duration)
    }
    
    private static var scrubbingHysteresis: Float = 0.01
    func approximatelyEqual(_ other: PlaybackPosition) -> Bool {
        return abs(percent - other.percent) < PlaybackPosition.scrubbingHysteresis
    }
}

extension PlaybackPosition: Comparable {
    static func ==(lhs: PlaybackPosition, rhs: PlaybackPosition) -> Bool {
        return lhs.percent == rhs.percent
    }
    
    static func <(lhs: PlaybackPosition, rhs: PlaybackPosition) -> Bool {
        return lhs.percent < rhs.percent
    }
}

private let positionFormatter: NumberFormatter = {
   let formatter = NumberFormatter()
    formatter.minimumIntegerDigits = 0
    formatter.minimumFractionDigits = 2
    formatter.maximumFractionDigits = 2
    return formatter
}()

extension PlaybackPosition: CustomStringConvertible {
    var description: String {
        return positionFormatter.string(from: NSNumber(value: percent)) ?? "—"
    }
}

private extension UILabel {
    func makeLabelMonospaced() {
        guard let baseFont = font else { return }

        let features = [
            [
                UIFontDescriptor.FeatureKey.featureIdentifier: kNumberSpacingType,
                UIFontDescriptor.FeatureKey.typeIdentifier: kMonospacedNumbersSelector
            ]
        ]
        let fontDescriptor = baseFont.fontDescriptor.addingAttributes([.featureSettings: features])
        font = UIFont(descriptor: fontDescriptor, size: 0)
    }
}

