//
//  CheckMarkViewController.swift
//  StatefulMachines
//
//  Created by Curt Clifton on 9/17/17.
//  Copyright © 2017 Curt Clifton. All rights reserved.
//

import UIKit
import SpriteKit

// simulate global storage
var modelIsChecked = false

class CheckMarkViewController: UIViewController {
    @IBOutlet weak var checkView: CheckBackgroundView!
    @IBOutlet weak var checkImage: UIImageView!
    @IBOutlet weak var particleView: SKView!
    @IBOutlet weak var panRecognizer: UIPanGestureRecognizer!
    @IBOutlet weak var stateLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        checkView.layer.borderWidth = 8
        checkView.layer.borderColor = UIColor(white: 0.4, alpha: 1.0).cgColor
        checkView.layer.cornerRadius = 16
        
        let size = particleView.bounds.size
        particleScene = SKScene(size: size)
        particleScene.backgroundColor = .clear
        particleNode = SKEmitterNode(fileNamed: "particle")!
        particleNode.position = CGPoint(x: size.width / 2, y: size.height)
        particleScene.addChild(particleNode)
        particleView.presentScene(particleScene)
        particleScene.isPaused = true
        
        state = .readingModelState
        updateControls()
    }

    @IBAction func gestureDidUpdate(_ pan: UIPanGestureRecognizer) {
        do {
            let region = try PanRegion(panRecognizer: pan)
            switch pan.state {
            case .began:
                beganSwipe(in: region)
                checkView.highlightedRegion = region
            case .changed:
                continuedSwipe(in: region)
                checkView.highlightedRegion = region
            case .ended:
                endedSwipe()
                checkView.highlightedRegion = nil
            case .failed:
                failedSwipe()
                checkView.highlightedRegion = nil
            case .possible, .cancelled: // we cancel ourselves to reset
                break
            }
        } catch is RangeError {
            endedSwipe()
            checkView.highlightedRegion = nil
        } catch {
            failedSwipe()
            checkView.highlightedRegion = nil
        }
    }
    
    // MARK: Triggers
    
    private func beganSwipe(in region: PanRegion) {
        switch state {
        case .unchecked
            where region == PanRegion(row: 2, column: 1):
            state = .checkSwipe(substate: .hand)
        case .checked
            where region.column == 3:
            state = .uncheckSwipe(substate: .right)
        default:
            // ignore all others
            break
        }
    }
    
    private func continuedSwipe(in region: PanRegion) {
        switch state {
        case .checkSwipe(substate: .hand)
            where region == PanRegion(row:3, column: 2):
            state = .checkSwipe(substate: .elbow)
        case .checkSwipe(substate: .elbow)
            where region == PanRegion(row: 1, column: 3):
            state = .checkSwipe(substate: .shoulder)
        case .checkSwipe(substate: .elbow)
            where region.column == 1:
            state = .unchecked
        case .checkSwipe(substate: .shoulder)
            where region.column < 3:
            state = .unchecked
        case .checkSwipe(substate: .shoulder)
            where region == PanRegion(row: 2, column: 3):
            state = .unchecked
            
        case .uncheckSwipe(substate: .right)
            where region.column < 3:
            state = .uncheckSwipe(substate: .middle)
        case .uncheckSwipe(substate: .middle)
            where region.column < 2:
            state = .uncheckSwipe(substate: .left)
        case .uncheckSwipe(substate: .middle)
            where region.column > 2:
            state = .checked
        case .uncheckSwipe(substate: .left)
            where region.column > 1:
            state = .checked
        default:
            break
        }
    }
    
    private func endedSwipe() {
        switch state {
        case .checkSwipe(substate: .shoulder):
            state = .checking
        case .checkSwipe:
            state = .unchecked
        case .uncheckSwipe(substate: .left):
            state = .unchecking
        case .uncheckSwipe:
            state = .checked
        default:
            break
        }
    }
    
    private func failedSwipe() {
        switch state {
        case .checkSwipe:
            state = .unchecked
        case .uncheckSwipe:
            state = .checked
        default:
            break
        }
    }

    // MARK: Private API
    private var particleScene: SKScene!
    private var particleNode: SKEmitterNode!
    
    private var state: State = .initial {
        didSet {
            guard state != oldValue else { return }
            
            switch state {
            case .initial:
                break
            case .readingModelState:
                if (modelIsChecked) {
                    self.thenSetState(to: .checked)
                } else {
                    self.thenSetState(to: .unchecked)
                }
            case .unchecking:
                modelIsChecked = false
                panRecognizer.isEnabled = false
                UIView.animate(withDuration: 0.2, animations: {
                    self.checkImage.alpha = 0
                }, completion: { _ in
                    self.thenSetState(to: .unchecked)
                })
            case .unchecked:
                resetGestureRecognizer()
                updateControls()
            case .checking:
                modelIsChecked = true
                panRecognizer.isEnabled = false
                particleScene.isPaused = false
                particleNode.resetSimulation()
                UIView.animate(withDuration: 1.5, animations: {
                    self.checkImage.alpha = 1
                }, completion: { _ in
                    self.particleScene.isPaused = true
                    self.thenSetState(to: .checked)
                })
            case .checked:
                resetGestureRecognizer()
                updateControls()
            case .checkSwipe:
                break
            case .uncheckSwipe:
                break
            }
            
            stateLabel.text = String(describing: state)
        }
    }
    
    private func thenSetState(to state: State) {
        DispatchQueue.main.async {
            self.state = state
        }
    }
    
    private func resetGestureRecognizer() {
        panRecognizer.isEnabled = false
        
        // delay a turn so gesture callbacks flush before we clear the highlight
        DispatchQueue.main.async {
            self.checkView.highlightedRegion = nil
            self.panRecognizer.isEnabled = true
        }
    }
    
    private func updateControls() {
        switch state {
        case .checked:
            checkImage.alpha = 1
        default:
            checkImage.alpha = 0
        }
    }
}

private enum State {
    case initial
    case readingModelState
    
    case unchecking
    case unchecked
    
    case checking
    case checked
    
    case checkSwipe(substate: CheckSwipeState)
    case uncheckSwipe(substate: UncheckSwipeState)
}

extension State: Equatable {
    static func ==(lhs: State, rhs: State) -> Bool {
        switch (lhs, rhs) {
        case (.initial, .initial): return true
        case (.unchecking, .unchecking): return true
        case (.unchecked, .unchecked): return true
        case (.checking, .checking): return true
        case (.checked, .checked): return true
        case let (.checkSwipe(lhsSubstate), .checkSwipe(rhsSubstate)) where lhsSubstate == rhsSubstate:
            return true
        case let (.uncheckSwipe(lhsSubstate), .uncheckSwipe(rhsSubstate)) where lhsSubstate == rhsSubstate:
            return true
        default:
            return false
        }
    }
}

extension State: CustomStringConvertible {
    var description: String {
        switch self {
        case .initial: return "initial"
        case .readingModelState: return "reading model"
        case .unchecking: return "unchecking"
        case .unchecked: return "unchecked"
        case .checking: return "checking"
        case .checked: return "checked"
        case .checkSwipe(let substate):
            return "checking \(substate)"
        case .uncheckSwipe(let substate):
            return "unchecking \(substate)"
        }
    }
}

private enum CheckSwipeState {
    case hand
    case elbow
    case shoulder
}

private enum UncheckSwipeState {
    case right
    case middle
    case left
}

