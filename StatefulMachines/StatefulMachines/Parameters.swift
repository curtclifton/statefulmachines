//
//  Parameters.swift
//  StatefulMachines
//
//  Created by Curt Clifton on 9/9/17.
//  Copyright © 2017 Curt Clifton. All rights reserved.
//

import Foundation

private let lookBehindTheCurtain = false

struct Parameters {
    
    static let imageSetDelay: TimeInterval = 2
    static let thumbnailDelay: TimeInterval = 3
    static let fullResolutionDelay: TimeInterval = 4
    static let delayRandomizationRange: TimeInterval = lookBehindTheCurtain ? 0 : 3.5
    static let shouldUseSingleImage = lookBehindTheCurtain
    
    static let scrubbingSettlingTime: TimeInterval = 0.2
    static let shouldShowStateLabel = lookBehindTheCurtain
    
    static let shouldShowRegions = lookBehindTheCurtain
}
