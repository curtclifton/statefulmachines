//
//  ImageVendor.swift
//  StatefulMachines
//
//  Created by Curt Clifton on 9/9/17.
//  Copyright © 2017 Curt Clifton. All rights reserved.
//

import Foundation
import UIKit
import GameplayKit

typealias ImageID = Int

// MARK: -
struct ImageSet {
    let imageVendors: [ImageVendor]
}

protocol ImageVendorDelegate: class {
    func downloading(id: ImageID)
    func receivedThumbnail(_ thumbnail: UIImage, for id: ImageID)
    func receivedImage(_ fullResolutionImage: UIImage, for id: ImageID)
}

// MARK: -
/// A façade to simulate a slow network connection for fetching a single image.
///
/// Actually pre-loads everything from local storage and then simulates the delays.
class ImageVendor {
    weak var delegate: ImageVendorDelegate? {
        didSet {
            self.killSwitch.trip() // trip old kill switch

            // delay so these state changes are queued behind any queued by `state` itself
            DispatchQueue.main.async {
                if self.delegate == nil {
                    self.state = .idle
                } else {
                    self.killSwitch = KillSwitch() // make new kill switch for new delegate
                    self.state = .newDelegate
                }
            }
        }
    }
    
    // MARK: Private API
    private let id: ImageID
    private let image: Image
    private var killSwitch: KillSwitch = KillSwitch()
    private var state: ShadowState = .idle {
        didSet {
            switch state {
                
            case .idle:
                // nothing to do
                break
                
            case .newDelegate:
                delegate?.downloading(id: id)
                DispatchQueue.main.async { // need to defer since assigning to `state` in didSet handler doesn't re-provoke didSet handler
                    self.state = .waitingForThumbnail
                }
                
            case .waitingForThumbnail:
                afterRandomDelay(around: Parameters.thumbnailDelay, killSwitch: killSwitch) {
                    self.state = .downloadedThumbnail
                }
                
            case .downloadedThumbnail:
                delegate?.receivedThumbnail(image.thumbnail, for: id)
                DispatchQueue.main.async {
                    self.state = .waitingForFullResImage
                }
                
            case .waitingForFullResImage:
                afterRandomDelay(around: Parameters.fullResolutionDelay, killSwitch: killSwitch) {
                    self.state = .downloadedFullResImage
                }

            case .downloadedFullResImage:
                delegate?.receivedImage(image.fullResolution, for: id)
                DispatchQueue.main.async {
                    self.state = .idle
                }
                
            }
        }
    }
    
    fileprivate init(id: ImageID, image: Image) {
        self.id = id
        self.image = image
    }
    
    /// State machine to simulate slow network connection.
    private enum ShadowState {
        case newDelegate
        case waitingForThumbnail
        case downloadedThumbnail
        case waitingForFullResImage
        case downloadedFullResImage
        case idle
    }
}

/// A façade to simulate a slow network connection for fetching images.
///
/// Actually pre-loads everything from local storage and then simulates the delays.
struct ImagesVendor {
    init() {
        // demo code, just ! things home
        let bundle = Bundle.main
        let fullResolutionURLs = bundle.urls(forResourcesWithExtension: "jpg", subdirectory: "Photos")!
        let thumbnailURLs = bundle.urls(forResourcesWithExtension: "jpg", subdirectory: "Thumbnails")!
        assert(fullResolutionURLs.count == thumbnailURLs.count)
        
        let fullResolutionImages = loadImages(from: fullResolutionURLs)
        let thumbnailImages = loadImages(from: thumbnailURLs)

        self.images = zip(fullResolutionImages, thumbnailImages).map { Image(fullResolution: $0, thumbnail: $1) }
    }
    
    /// Fetches the user's image set.
    ///
    /// - Parameter callback: invoked on the main thread with the result image set
    func fetchImageSet(_ callback: @escaping (ImageSet) -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + Parameters.imageSetDelay) {
            if Parameters.shouldUseSingleImage {
                // just one for debugging
                callback(ImageSet(imageVendors: [self.imageSet.imageVendors.first!]))
            } else {
                callback(self.imageSet)
            }
        }
    }
    
    // MARK: Private API

    private let images: [Image]
    
    private var imageSet: ImageSet {
        return ImageSet(imageVendors: images.enumerated().map({
            let (id, image) = $0
            return ImageVendor(id: id, image: image)
        }))
    }
}

private func loadImages(from urls: [URL]) -> [UIImage] {
    // demo code, just ! things home
    let result: [UIImage] = urls.map { url in
        let data = try! Data(contentsOf: url)
        let image = UIImage(data: data)!
        return image
    }
    return result
}

let randomDistribution = GKRandomDistribution(forDieWithSideCount: 100)

private func afterRandomDelay(around nominalDelay: TimeInterval, killSwitch: KillSwitch, execute callback: @escaping () -> Void) {
    let randomRatio = Double(randomDistribution.nextUniform())
    let delayDelta = Parameters.delayRandomizationRange * randomRatio - (Parameters.delayRandomizationRange / 2)
    let delay = max(nominalDelay + delayDelta, 0)
    DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
        guard !killSwitch.isTripped else {
            print("☠—killed")
            return
        }
        callback()
    }
}

private struct Image {
    let fullResolution: UIImage
    let thumbnail: UIImage
}

private class KillSwitch {
    private(set) var isTripped = false
    func trip() {
        isTripped = true
    }
}
