//
//  ImageCollectionViewCell.swift
//  StatefulMachines
//
//  Created by Curt Clifton on 9/9/17.
//  Copyright © 2017 Curt Clifton. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView! {
        didSet {
            contentView.layer.cornerRadius = 6
            contentView.clipsToBounds = true
        }
    }
    @IBOutlet weak var stateLabel: UILabel!
    
    var imageVendor: ImageVendor? {
        didSet {
            state = .pending
            if let vendor = imageVendor {
                vendor.delegate = self
            } else {
                oldValue?.delegate = nil
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        stateLabel.isHidden = !Parameters.shouldShowStateLabel
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageVendor = nil
    }
    
    // MARK: Private API
    
    private var state: State = .pending {
        didSet {
            guard state != oldValue else { return }
            
            stateLabel.text = String(describing: state)
            
            switch state {
            case .pending:
                imageView.image = nil // assign directly to avoid delay on reuse
            case .loading:
                image = #imageLiteral(resourceName: "placeholder")
            case let .thumbnail(_, image):
                self.image = image
            case let .full(_, image):
                self.image = image
            }
        }
    }
    
    private var image: UIImage? {
        set {
            UIView.transition(with: imageView, duration: 0.15, options: .transitionCrossDissolve, animations: {
                self.imageView.image = newValue
            }, completion: nil)
        }
        get {
            return imageView.image
        }
    }
}

extension ImageCollectionViewCell: ImageVendorDelegate {
    func downloading(id: ImageID) {
        state = .loading(id: id)
    }
    
    func receivedThumbnail(_ thumbnail: UIImage, for id: ImageID) {
        state = .thumbnail(id: id, image: thumbnail)
    }
    
    func receivedImage(_ fullResolutionImage: UIImage, for id: ImageID) {
        state = .full(id: id, image: fullResolutionImage)
    }
}

private enum State {
    case pending
    case loading(id: ImageID)
    case thumbnail(id: ImageID, image: UIImage)
    case full(id: ImageID, image: UIImage)
}

extension State: Equatable {
    static func ==(lhs: State, rhs: State) -> Bool {
        switch (lhs, rhs) {
        case (.pending, .pending): return true
        case let (.loading(lhsID), .loading(rhsID)) where lhsID == rhsID: return true
        case let (.thumbnail(lhsID, lhsImage), .thumbnail(rhsID, rhsImage)) where lhsID == rhsID && lhsImage === rhsImage: return true
        case let (.full(lhsID, lhsImage), .full(rhsID, rhsImage)) where lhsID == rhsID && lhsImage === rhsImage: return true
        default: return false
        }
    }
}

extension State: CustomStringConvertible {
    var description: String {
        switch self {
        case .pending: return "pending"
        case .loading: return "loading"
        case .thumbnail: return "thumbnail"
        case .full: return "full"
        }
    }
}
