//
//  CheckBackgroundView.swift
//  StatefulMachines
//
//  Created by Curt Clifton on 9/20/17.
//  Copyright © 2017 Curt Clifton. All rights reserved.
//

import UIKit

private let highlightColor = UIColor.orange.withAlphaComponent(0.5)

class CheckBackgroundView: UIView {
    
    var highlightedRegion: PanRegion? = nil {
        didSet {
            if highlightedRegion != oldValue {
                setNeedsDisplay()
            }
        }
    }

    override func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext() else { return }
        UIColor.clear.setFill()
        context.fill(rect)
        
        if Parameters.shouldShowRegions, let region = highlightedRegion {
            let horizontalSplit = CGFloat(region.row - 1) * rect.height / PanRegion.divisions
            let (_, bottomRect) = rect.divided(atDistance: horizontalSplit, from: .minYEdge)
            let (rowRect, _) = bottomRect.divided(atDistance: rect.height / PanRegion.divisions, from: .minYEdge)
            let verticalSplit = CGFloat(region.column - 1) * rect.width / PanRegion.divisions
            let (_, rightRect) = rowRect.divided(atDistance: verticalSplit, from: .minXEdge)
            let (regionRect, _) = rightRect.divided(atDistance: rect.width / PanRegion.divisions, from: .minXEdge)

            highlightColor.setFill()
            context.fill(regionRect)
        }
    }

}
