//
//  ImageCollectionViewController.swift
//  StatefulMachines
//
//  Created by Curt Clifton on 9/9/17.
//  Copyright © 2017 Curt Clifton. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class ImageCollectionViewController: UICollectionViewController {
    @IBOutlet weak var activityIndicatorContainer: UIView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    var imageSet: ImageSet? {
        didSet {
            updateView()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        updateView()
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return (imageSet == nil) ? 0 : 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let imageSet = imageSet else { return 0 }
        return imageSet.imageVendors.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
    
        guard let imageCell = cell as? ImageCollectionViewCell else {
            assertionFailure("misconfigured storyboard, cell class “\(type(of: cell))”")
            return cell
        }
        
        guard let imageSet = self.imageSet else {
            assertionFailure("unexpected nil imageSet")
            return cell
        }
        
        let vendor = imageSet.imageVendors[indexPath.row]
        imageCell.imageVendor = vendor
        
        return imageCell
    }

    // MARK: Private API
    
    private func updateView() {
        guard isViewLoaded else { return }
        
        if imageSet == nil {
            self.collectionView?.reloadData()
            self.spinner.startAnimating()
            UIView.animate(withDuration: 0.25, animations: {
                self.activityIndicatorContainer.alpha = 1
            })
        } else {
            UIView.animate(withDuration: 0.25, animations: {
                self.activityIndicatorContainer.alpha = 0
                self.collectionView?.reloadData()
            }, completion: { _ in
                self.spinner.stopAnimating()
            })
        }
    }
}
