//
//  PanRegions.swift
//  StatefulMachines
//
//  Created by Curt Clifton on 9/20/17.
//  Copyright © 2017 Curt Clifton. All rights reserved.
//

import UIKit

struct PanRegion {
    static let divisions: CGFloat = 3
    
    /// In 1...divisions
    let row: Int
    /// In 1...divisions
    let column: Int
    
    init(panRecognizer: UIPanGestureRecognizer) throws {
        guard let view = panRecognizer.view else {
            fatalError()
        }
        
        let location = panRecognizer.location(in: view)
        column = try PanRegion.sliceIndex(for: location.x, in: view.bounds.minX...view.bounds.maxX)
        row = try PanRegion.sliceIndex(for: location.y, in: view.bounds.minY...view.bounds.maxY)
    }
    
    init(row: Int, column: Int) {
        self.row = row
        self.column = column
    }
    
    private static func sliceIndex(for location: CGFloat, in range: ClosedRange<CGFloat>) throws -> Int {
        guard range.contains(location) else {
            throw RangeError.outOfBounds
        }
        let normalizedLocation = (location - range.lowerBound) / (range.upperBound - range.lowerBound)
        let result = Int(floor(normalizedLocation * divisions)) + 1
        return result
    }
}

extension PanRegion: Equatable {
    static func ==(lhs: PanRegion, rhs: PanRegion) -> Bool {
        return lhs.row == rhs.row && lhs.column == rhs.column
    }
}

enum RangeError: Error {
    case outOfBounds
}
